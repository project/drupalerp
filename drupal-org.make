core = 6.15

;projects[drupal] = 6.15

projects[autologout] = 2.4
projects[admin_menu] = 1.5
projects[advanced_help] = 1.2
projects[calendar] = 2.2
projects[countdowntimer] = 2.21
projects[date] = 2.4
projects[dirtyforms] = 1.1
projects[gmap] = 1.0
projects[install_profile_api] = 2.1
projects[nice_menus] = 1.3
projects[onbeforeunload] = 1.0
projects[print] = 1.10
projects[quicktabs] = 2.0-rc3
;projects[services] = 2.x-dev
projects[token] = 1.12
;projects[ubercart] = 2.2
projects[views] = 2.8
projects[workflow] = 1.3

projects[erp] = 1.1-beta7
projects[erp_theme] = 1.1-beta1
